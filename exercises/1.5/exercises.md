# Exercises 1.5

## Problem overview

- 1
- 3
- 5
- 7
- 11
- 23
- 29-33

## Problem 1
## Problem 3
## Problem 5
## Problem 7
## Problem 11
## Problem 23

a. Verdict: True.
   A homogenous system always has atleast one solution
b. Verdict: False.
   Ax = 0 is implicit as 0 is substituting the real solution
c. Verdict: False
   A system is non-trivial if it has a free variable otherwise
d. Verdict: False.
   x = p + tv the line is parallel to v as t is the translator
e. Verdict: False 
   The solution could also produce an empty set. Implying Ap = b for p as a solution vector

## Problem 29

Doesn't produce a non-trivial vector as there is no free variable for  3x3 with pivots

## Problem 30

Two pivots would produce a free variable

## Problem 31

A 3 x 2 means that each column has a pivot and and there is therefor no free variables and it doesn't produce a non-trivial solution

## Problem 32

A 2 x 4 matrix will have 2 columns with pivots and two free meaning that it has two basic variables and can be considered a nontrivial solution

## Problem 33

A[:,1] is two times A[:,0] and x = [3,-1] to produce the result seen above for Ax = 0















