# Exercises 2.3

1-4, 11, 12, 15, 16

## Problems

### Problem 1

### Problem 11

- Verdict: true, There are no free variables so it's true.
- Verdict: true, If A spans R^n then A must have a solution b which is unique
- Verdict: false, te statement is true only for invertible matrices, matrices that have a unique solution.
- Verdict: true, A has free variables then. 
- Verdict: true, according to statement if A is not invertible then A^T isn't either

### Problem 12

- Verdict: True
- Verdict: True,
- Verdict: True,
- Verdict: False, we can't be sure that it has a pivot, even if it maps
- Verdict: True, if Ax = b is inconsistent, then there must be a free variable and the transformation is not on to one.

### Problem 15

A singular matrix cannot be invertible, it will either be inconsistent or have free variables.

### Problem 16

According it IMT if one rule doesn't apply none does. This means that if it doesn't span R^n, then it isn't invertible.
