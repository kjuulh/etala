# Exercises 2.2

1, 5, 9, 30, 31

## Problems

### Problem 9

a. In order for a matrix B to be inverse of A, both equations AB and BA = I must be true
   Verdict: True. By definition A^-1A = I
b. Verdict: False. See picture
c. Verdict: False. The determinant can be caluclated according to picture, however if we have a singular matrix, the determinant will be 0.
d. Verdict: True, Ax = b is unique for each b in R^n 
e. Verdict: True, Each matrix than can have pivots in each column are invertible.

### Problem 30


