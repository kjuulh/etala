# Exercise 1.4

## Problems Overview

- 1-4
- 9
- 11
- 21
- 23
- 25
- 31
- 32
- 40
- 42

## Problem 1

## Problem 2

## Problem 3

## Problem 4

## Problem 9

## Problem 11

## Problem 21

## Problem 23

a. Verdict: False.
   The equation is a matrix equation as A is commonly a matrix
b. Verdict: True
   A vector b is a linear combination of the columns of a matrix A if the equation Ax = b has a solution.
c. Verdict: False.
   An augmented matrix will produce an inconsistent result if the statement should be true. a coefficient matrix should have a pivot in each row.
d. Verdict: True.
   It is commonly called the dot-product
e. Verdict: True.
   True, if each column correspons to R² then it's true, as each row needs a pivot for it to be able to span R^m.
f. Verdict: True.
   False A can have a pivot, but the result can still be inconsistent. Row(3) = [0 1] = [0]

## Problem 25

## Problem 31

## Problem 32

## Problem 40

See jupyter *.ipynb

## Problem 42

